# Thalia
This is a tool for reporting worked hours and notes for student representatives at MDU.

This tool uses HTMX, htmxido and Sanic to be small and agile and cooool.

Read the pyproject.toml to see all the required packages.

## TODO
- [ ] Write this document
- [ ] Write a UX specification
- [ ] Write an API specification

## Installation

## Development
Let's use VirtualFish, Poetry and stuff for development.
- Create a virtual enviroment, for example: `vf new thalia`
- Activate it: `vf activate thalia`
- Use Poetry: `poetry install`
- Run a server with `poetry run uvicorn main:app --reload`

## UX Specification

## API Specification
