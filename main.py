from sanic import Sanic, html
from htmxido import domx as x

app = Sanic("Pythia")
app.static("/static/", "static")


@app.post("/clicked")
async def clicked(request):
    return html(str(
        x.div("Clicked!!!")
    ))


@app.get("/")
async def home(request):
    return html(str(
        x.html(
            x.head(
                x.script(src="https://unpkg.com/htmx.org@1.9.9"),
                # <link rel="stylesheet" href="https://unpkg.com/boltcss/bolt.min.css">
                x.link(href="https://unpkg.com/boltcss/bolt.min.css", rel="stylesheet"),
                x.link(href="static/toilet-paper.svg", rel="shortcut icon", type="image/svg"),
            ),
            x.body(
                x.h1("Running htmxido"),
                x.div(id="container")(
                    x.button("Reveal!!!", hxTarget="#container", hxTrigger="click", hxPost="/clicked")
                ),
            )
        )
    ))
